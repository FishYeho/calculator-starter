package ca.campbell.simplecalc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

//  DONE: add a field to input a 2nd number, get the input and use it in calculations
//  DONE: the inputType attribute forces a number keyboard, don't use it on the second field so you can see the difference

//  DONE: add buttons & methods for subtract, multiply, divide

//  DONE: add input validation: no divide by zero
//  DONE: input validation: set text to show error when it occurs (Done)

//  DONE: add a clear button that will clear the result & input fields

//  DONE: the hint for the result widget is hard coded, put it in the strings file

public class MainActivity extends Activity {
    EditText etNumber1;
    EditText etNumber2;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // get a handle on the text fields
        etNumber1 = (EditText) findViewById(R.id.num1);
        etNumber2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
    }  //onCreate()

    // TODO: replace with code that adds the two input numbers
    public void addNums(View v) {
        boolean isEmpty = checkInput(v);
        if (isEmpty == true) {
            result.setText("Please enter a number into each field");
        }
        else {
            double num1;
            double num2;
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            result.setText(Double.toString(num1 + num2));
        }
    }  //addNums()

    public void subtractNums(View v) {
        boolean isEmpty = checkInput(v);
        if (isEmpty == true) {
            result.setText("Please enter a number into each field");
        }
        else {
            double num1;
            double num2;
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            result.setText(Double.toString(num1 - num2));
        }
    }

    public void multiplyNums(View v) {
        boolean isEmpty = checkInput(v);
        if (isEmpty == true) {
            result.setText("Please enter a number into each field");
        }
        else {
            double num1;
            double num2;
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            result.setText(Double.toString(num1 * num2));
        }
    }

    public void divideNums(View v) {
        boolean isEmpty = checkInput(v);
        if (isEmpty == true) {
            result.setText("Please enter a number into each field");
        }
        else {
            double num1;
            double num2;
            num1 = Double.parseDouble(etNumber1.getText().toString());
            num2 = Double.parseDouble(etNumber2.getText().toString());
            if (num2 == 0) {
                result.setText("Cannot divide by zero!");
            }
            else {
                result.setText(Double.toString(num1 / num2));
            }
        }
    }

    public boolean checkInput (View v){
        if (etNumber1.getText().toString().matches("") || etNumber2.getText().toString().matches("")) {
            result.setText("Please enter a number into each field");
            return true;
        }
        return false;
    }

    public void clearText (View v) {
        etNumber1.setText(null);
        etNumber2.setText(null);
        result.setText("The answer should be here");
    }
}